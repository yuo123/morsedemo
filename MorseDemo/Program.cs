﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NAudio.Wave;

namespace MorseDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            MorseWaveProvider prov = new MorseWaveProvider(new WaveFormat(44100, 1));

            while (true)
            {
                prov.EncodeString(Console.ReadLine());
                prov.WriteToFile("sample.wav");

                using (WaveFileReader reader = new WaveFileReader(@"sample.wav"))
                    Console.WriteLine(MorseWaveDecoder.DecodeWave(reader));
            }
        }
    }
}
