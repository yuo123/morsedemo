﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NAudio.Wave;

namespace MorseDemo
{
    public class MorseWaveProvider : SyntheticWaveProvider
    {
        public const int UNITS_BETWEEN_SIGNALS = 1;
        public const int UNITS_BETWEEN_LETTERS = 3;
        public const int UNITS_BETWEEN_WORDS = 7;

        //see assignment for meaning
        protected static Dictionary<char, int[]> conversionTable;

        /// <summary>
        /// The duration, in seconds, of a single Morse unit
        /// </summary>
        private float unit;
        private float volume;
        private int frequency;

        public MorseWaveProvider(WaveFormat waveFormat) : base(waveFormat)
        {
            this.unit = 0.1f;
            this.volume = 1f;
            this.frequency = 800;

            this.BufferDuration = new TimeSpan(0, 2, 0);

            InitConversionTable();
        }

        static MorseWaveProvider()
        {
            InitConversionTable();
        }

        private static void InitConversionTable()
        {
            conversionTable = new Dictionary<char, int[]>();
            //https://en.wikipedia.org/wiki/Morse_code#/media/File:International_Morse_Code.svg
            //the character is on the left, the numbers in the array are beep durations.
            const int DIT = 1; //dot
            const int DAH = 3; //dash
            conversionTable.Add('1', new int[] { DIT, DAH, DAH, DAH, DAH });
            conversionTable.Add('2', new int[] { DIT, DIT, DAH, DAH, DAH });
            conversionTable.Add('3', new int[] { DIT, DIT, DIT, DAH, DAH });
            conversionTable.Add('4', new int[] { DIT, DIT, DIT, DIT, DAH });
            conversionTable.Add('5', new int[] { DIT, DIT, DIT, DIT, DIT });
            conversionTable.Add('6', new int[] { DAH, DIT, DIT, DIT, DIT });
            conversionTable.Add('7', new int[] { DAH, DAH, DIT, DIT, DIT });
            conversionTable.Add('8', new int[] { DAH, DAH, DAH, DIT, DIT });
            conversionTable.Add('9', new int[] { DAH, DAH, DAH, DAH, DIT });
            conversionTable.Add('0', new int[] { DAH, DAH, DAH, DAH, DAH });

            conversionTable.Add('A', new int[] { DIT, DAH });
            conversionTable.Add('B', new int[] { DAH, DIT, DIT, DIT });
            conversionTable.Add('C', new int[] { DAH, DIT, DAH, DIT });
            conversionTable.Add('D', new int[] { DAH, DIT, DIT });
            conversionTable.Add('E', new int[] { DIT });
            conversionTable.Add('F', new int[] { DIT, DIT, DAH, DIT });
            conversionTable.Add('G', new int[] { DAH, DAH, DIT });
            conversionTable.Add('H', new int[] { DIT, DIT, DIT, DIT });
            conversionTable.Add('I', new int[] { DIT, DIT });
            conversionTable.Add('J', new int[] { DIT, DAH, DAH, DAH });
            conversionTable.Add('K', new int[] { DAH, DIT, DAH });
            conversionTable.Add('L', new int[] { DIT, DAH, DIT, DIT });
            conversionTable.Add('M', new int[] { DAH, DAH });
            conversionTable.Add('N', new int[] { DAH, DIT });
            conversionTable.Add('O', new int[] { DAH, DAH, DAH });
            conversionTable.Add('P', new int[] { DIT, DAH, DAH, DIT });
            conversionTable.Add('Q', new int[] { DAH, DAH, DIT, DAH });
            conversionTable.Add('R', new int[] { DIT, DAH, DIT });
            conversionTable.Add('S', new int[] { DIT, DIT, DIT });
            conversionTable.Add('T', new int[] { DAH });
            conversionTable.Add('U', new int[] { DIT, DIT, DAH });
            conversionTable.Add('V', new int[] { DIT, DIT, DIT, DAH });
            conversionTable.Add('W', new int[] { DIT, DAH, DAH });
            conversionTable.Add('X', new int[] { DAH, DIT, DIT, DAH });
            conversionTable.Add('Y', new int[] { DAH, DIT, DAH, DAH });
            conversionTable.Add('Z', new int[] { DAH, DAH, DIT, DIT });

            conversionTable.Add('.', new int[] { DIT, DAH, DIT, DAH, DIT, DAH });
            conversionTable.Add('?', new int[] { DIT, DIT, DAH, DAH, DIT, DIT });
            conversionTable.Add('!', new int[] { DAH, DIT, DAH, DIT, DAH, DAH });
            conversionTable.Add('-', new int[] { DAH, DIT, DIT, DIT, DIT, DAH });
            conversionTable.Add('&', new int[] { DIT, DAH, DIT, DIT, DIT });

            conversionTable.Add(',', new int[] { DAH, DAH, DIT, DIT, DAH, DAH });
        }

        public void EncodeString(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                EncodeCharacter(s[i]);
                //the space between letters is 3 units, 2 + 1 (the space after the last letter)
                EncodeUnits(UNITS_BETWEEN_LETTERS - UNITS_BETWEEN_SIGNALS, 0);
            }
        }

        public void EncodeCharacter(char c)
        {
            if (c == ' ')
                //the space between words is seven units, 3 + 2 + 2 (the space after the last letter and this one)
                EncodeUnits(UNITS_BETWEEN_WORDS - (2 * UNITS_BETWEEN_LETTERS) + UNITS_BETWEEN_SIGNALS, 0);
            else
            {
                int[] units;
                //characters not in the conversion table will be ignored
                if (conversionTable.TryGetValue(char.ToUpper(c), out units))
                    EncodeCharacter(units);
            }
        }

        public void EncodeCharacter(int[] units)
        {
            for (int i = 0; i < units.Length; i++)
            {
                EncodeUnits(units[i], this.volume);
                //the space between letters of the same word is 1 unit
                EncodeUnits(UNITS_BETWEEN_SIGNALS, 0);
            }
        }

        public void EncodeUnits(int units, float volume)
        {
            this.SineWave(volume, this.unit * units, this.frequency);
        }

        public static char DecodeChar(IEnumerable<int> units)
        {
            foreach (KeyValuePair<char, int[]> entry in conversionTable)
                if (entry.Value.SequenceEqual(units))
                    return entry.Key;

            return '�';
        }
    }
}
