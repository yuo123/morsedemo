﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NAudio;
using NAudio.Wave;
using NAudio.Utils;

namespace MorseDemo
{
    public class MorseWaveDecoder
    {
        private const int VOLUME_SEARCH_LENGTH = 10;
        private const int REQUIRED_ABOVE_THRESHOLD = 2;
        private const float VOLUME_THRESHOLD = 0.01f;

        /// <summary>
        /// Returns the lengths of segments (in samples) of quiet and noise, starting from quiet and alternating
        /// </summary>
        public static IEnumerable<int> WaveToSegments(IWaveProvider src)
        {
            return SamplesToSegments(src.ToSampleProvider());
        }
        /// <summary>
        /// Returns the lengths of segments (in samples) of quiet and noise, starting from quiet and alternating
        /// </summary>
        public static IEnumerable<int> SamplesToSegments(ISampleProvider src)
        {
            GenericCircularBuffer<float> buffer = new GenericCircularBuffer<float>(VOLUME_SEARCH_LENGTH);
            float[] demiBuffer = new float[1];
            //samples since the last change
            int elapsed = 0;
            bool noisy = false;
            //read one byte at a time
            while (src.Read(demiBuffer, 0, 1) > 0)
            {
                buffer.Write(demiBuffer[0]);
                //get the amount of samples above the threshold
                int above = buffer.Count(sample => Math.Abs(sample) > VOLUME_THRESHOLD);
                if ((noisy && above < REQUIRED_ABOVE_THRESHOLD) || (!noisy && above >= REQUIRED_ABOVE_THRESHOLD))
                {
                    //change detected
                    noisy = !noisy;
                    if (elapsed <= buffer.Size)
                        yield return 0;
                    else
                        yield return elapsed;
                    elapsed = 0;
                }
                else
                    elapsed++;
            }
        }

        /// <summary>
        /// Finds the Morse unit from a silence-noise segment list and returns the segments as units
        /// </summary>
        public static int[] NormalizeSegments(IEnumerable<int> segments)
        {
            //a list is required because we have two passes
            return NormalizeSegments(segments.ToList());
        }
        /// <summary>
        /// Finds the Morse unit from a silence-noise segment list and returns the segments as units
        /// </summary>
        public static int[] NormalizeSegments(List<int> segments)
        {
            int[] norm = new int[segments.Count];
            //a single Morse unit - should be the shortest segment (there is a unit of silence between each dot or dash)
            //using double to round better
            double unit = MinAbove(segments, VOLUME_SEARCH_LENGTH);
            for (int i = 0; i < segments.Count; i++)
                norm[i] = (int)Math.Round(segments[i] / unit);

            return norm;
        }

        private static int MinAbove(IEnumerable<int> items, int threshold)
        {
            int min = int.MaxValue;
            foreach (int num in items)
                if (num < min && num >= threshold)
                    min = num;
            return min;
        }

        public static string DecodeUnits(int[] units)
        {
            StringBuilder sb = new StringBuilder();
            int prev = 1;
            for (int i = 2; i < units.Length; i += 2) //i+=2 because we only check for spaces, and i=3 to ignore leading silence
            {
                //space between letters or words
                if (units[i] >= MorseWaveProvider.UNITS_BETWEEN_LETTERS)
                {
                    if (i != prev)
                    {
                        //end of letter
                        sb.Append(MorseWaveProvider.DecodeChar(SelectRange(units, prev, i, 2)));
                        if (units[i] >= MorseWaveProvider.UNITS_BETWEEN_WORDS)
                            //end of word
                            sb.Append(' ');
                    }
                    prev = i + 1;
                }
            }
            //decode the last letter
            if (prev != units.Length - 1)
                sb.Append(MorseWaveProvider.DecodeChar(SelectRange(units, prev, units.Length - 1, 2)));

            return sb.ToString();
        }

        public static string DecodeWave(IWaveProvider src)
        {
            return DecodeUnits(NormalizeSegments(WaveToSegments(src)));
        }

        private static IEnumerable<T> SelectRange<T>(T[] arr, int start, int end, int increment = 1)
        {
            for (int i = start; i <= end; i += increment)
                yield return arr[i];
        }
    }
}
