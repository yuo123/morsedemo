﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseDemo
{
    public class GenericCircularBuffer<T> : IEnumerable<T>
    {
        private T[] m_buffer;
        private int m_pos;

        public T this[int index]
        {
            get
            {
                try
                {
                    return m_buffer[(m_pos + index) % this.Size];
                }
                catch (ArgumentOutOfRangeException)
                {
                    throw new ArgumentOutOfRangeException("index", "Index must be non-negative");
                }
            }
        }

        public int Size
        {
            get
            {
                return m_buffer.Length;
            }
        }

        public GenericCircularBuffer(int size)
        {
            m_buffer = new T[size];
            m_pos = 0;
        }

        public void Write(T value)
        {
            //the previous first will be the new last
            m_buffer[m_pos] = value;
            //advance m_pos
            m_pos = (m_pos + 1) % this.Size;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < this.Size; i++)
                yield return this[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
