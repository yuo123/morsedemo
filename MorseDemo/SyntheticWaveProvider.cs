﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NAudio.Wave;

namespace MorseDemo
{
    /// <summary>
    /// A <see cref="BufferedWaveProvider"/> that contains methods to generate synthetic sound
    /// </summary>
    public class SyntheticWaveProvider : BufferedWaveProvider
    {
        public SyntheticWaveProvider(WaveFormat waveFormat) : base(waveFormat) { }

        public void SineWave(float volume, float seconds, int freq)
        {
            float[] samples = new float[(int)Math.Round(this.WaveFormat.SampleRate * seconds)];
            //the coefficient of i when applying the sinus function
            // freq / WaveFormat.SampleRate - portion of a sample out of a single period
            // * Math.PI * 2 - portion to radians
            double coeff = (double)freq / WaveFormat.SampleRate * Math.PI * 2;
            for (int i = 0; i < samples.Length; i++)
                samples[i] = (float)Math.Sin(i * coeff) * volume;
            this.AddSamples(samples);
        }

        /// <summary>
        /// Generates a square wave with the specified volume and the specified frequency for the specified duration
        /// </summary>
        /// <param name="freq">The frequency of the wave, in Hertz</param>
        public void SquareWave(float volume, float seconds, int freq)
        {
            float pulseDuration = 1f / freq;
            int pulsePairs = (int)Math.Round(seconds * freq / 2);
            for (int i = 0; i < pulsePairs; i++)
            {
                FlatPulse(volume, pulseDuration);
                FlatPulse(-volume, pulseDuration);
            }
        }

        /// <summary>
        /// Generates constant-level audio of the specified volume for a specified duration
        /// </summary>
        public void FlatPulse(float volume, float seconds)
        {
            float[] samples = new float[(int)Math.Round(this.WaveFormat.SampleRate * seconds)];
            for (int i = 0; i < samples.Length; i++)
                samples[i] = volume;
            this.AddSamples(samples);
        }

        /// <summary>
        /// Adds the all the samples in a given array, in a normalized form. Values must be between -1 and 1.
        /// </summary>
        public void AddSamples(float[] samples)
        {
            byte[] pcm = new byte[samples.Length * 2];
            ConvertSamplesTo16BitPcm(samples, pcm);
            this.AddSamples(pcm, 0, pcm.Length);
        }

        //adapted from http://stackoverflow.com/a/42151979
        /// <summary>
        /// Converts normalized float samples int range [-1, 1] to PCM samples
        /// </summary>
        /// <param name="samples">An array containing the source samples, in range [-1, 1]</param>
        /// <param name="pcm">An array to output the resulting PCM bytes to. Must be twice the size of <paramref name="samples"/></param>
        protected static void ConvertSamplesTo16BitPcm(float[] samples, byte[] pcm)
        {
            int sampleIndex = 0;
            int pcmIndex = 0;

            while (sampleIndex < samples.Length)
            {
                //outsample will be between short.MinValue to short.MaxValue (short = Int16)
                short outsample = (short)(samples[sampleIndex] * short.MaxValue);
                //split the 16-bit sample to two (8-bit) bytes
                pcm[pcmIndex] = (byte)(outsample & 0xff);
                pcm[pcmIndex + 1] = (byte)((outsample >> 8) & 0xff);

                sampleIndex++;
                pcmIndex += 2;
            }
        }

        public void WriteToFile(string path)
        {
            using (WaveFileWriter writer = new WaveFileWriter(path, this.WaveFormat))
            {
                byte[] buffer = new byte[256];
                while (this.BufferedBytes > 0)
                {
                    this.Read(buffer, 0, buffer.Length);
                    writer.Write(buffer, 0, buffer.Length);
                }
            }
        }
    }
}
